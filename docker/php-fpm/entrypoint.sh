#!/bin/bash

# We have to add ACLs for PHP to write to the disk
setfacl -R -m u:www-data:rwx /var/www/html &> /dev/null

_term() {
  echo "Caught SIGTERM signal!"
  kill -TERM "$child" 2>/dev/null
  exit 0;
}

# When Docker says "shutdown", we tell php-fpm to kick rocks.
trap _term SIGTERM

echo "Starting PHP FPM...";
php-fpm7.0 &

# Get the child process ID for php-fpm
child=$!
wait "$child"

<div class="navmenu navmenu-default navmenu-fixed-right offcanvas">
	<div class="navmenu-brand navmenu-close-button text-right" data-toggle="offcanvas" data-target=".navmenu">
		<img src="{{ asset('images/header/closeIcon.svg') }}" alt="Menu Icon" height="40px">
	</div>
	<div class="navmenu-body">
		<ul class="nav navmenu-nav bottom-cta">
            Hello
		</ul>
		<ul class="nav navmenu-nav">
            Links
		</ul>
	</div>
</div>

<nav class="navbar-default">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6">
                <img src="{{ asset('/images/header/ASAP.svg') }}" alt="ASAP Logo">
            </div>
            <div class="d-none d-md-block col-md-6">
                <ul>
                    <li><a class="scrollTo" href="#services">Services</a></li>
                    <li><a class="scrollTo" href="#works">Works</a></li>
                    <li><a class="scrollTo" href="#about_contact">About</a></li>
                    <li><a class="scrollTo" href="#contact">Contact</a></li>
                </ul>
            </div>
            <div class="col-6 d-md-none text-right">
                <button class="navbar-toggler" type="button" data-toggle="offcanvas" data-target="#myNavmenu" >
                    <img src="{{ asset('/images/icons/sidenav-icon.svg') }}" alt="Open SideNav">
                  </button>
            </div>
        </div>
    </div>
</nav>

<nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-right offcanvas canvas-slid" role="navigation">
        <button class="navmenu-close-button" type="button" data-toggle="offcanvas" data-target="#myNavmenu" >
            <img src="{{ asset('/images/icons/close-icon.svg') }}" alt="Close Icon">
        </button>
    <h4>Menu</h4>
    <ul>
        <li><a class="scrollTo" href="#services">Services</a></li>
        <li><a class="scrollTo" href="#works">Works</a></li>
        <li><a class="scrollTo" href="#about_contact">About</a></li>
        <li><a class="scrollTo" href="#contact">Contact</a></li>
    </ul>
    <div class="call-link">
        <a href="tel:3608367200"><img src="{{ asset('images/icons/phone-icon-2.svg') }}" alt="Phone Icon">(360) 836-7200</a>
    </div>
</nav>
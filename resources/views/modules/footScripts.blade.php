<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>

<script src="{{ asset('../js/material/material.js') }}"></script>
<script src="{{ asset('../js/jquery-sticky/sticky.min.js') }}"></script>
<script>
    $.material.init();

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()

        $('.toggle-button').click(function () {
            $('.toggle-columns').toggle();
        });

        $('.sticky').stick_in_parent();

        $(".scrollTo").on('click', function(e) {
            e.preventDefault();
            if($('.offcanvas').hasClass('in')) {
                $('.navmenu-close-button').click();
            }
            var target = $(this).attr('href');
            $('html, body').animate({
            scrollTop: ($(target).offset().top)
            }, 500);
        });

        $('.scroll-up').click(function() {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        })
    })
</script>
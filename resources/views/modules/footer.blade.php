<div class="container">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <img src="{{ asset('images/icons/facebook-icon.svg') }}" alt="Facebook">
            <img src="{{ asset('images/icons/instagram-icon.svg') }}" alt="Instagra,">
            <img src="{{ asset('images/icons/linkedin-icon.svg') }}" alt="Linked In">
            <img src="{{ asset('images/icons/youtube-icon.svg') }}" alt="YouTube">
        </div>
        <div class="col-12 col-sm text-sm-left order-sm-first">
                <p>&copy; 2019 Asap Maintenance, LLC. <br class="d-sm-none" />All Rights Reserved.</p>
            </div>
        <div class="d-none d-sm-block col-sm-1 col-md-2 col-lg-3 text-right">
            <img class="scroll-up" src="{{ asset('images/icons/scroll-up-icon.svg') }}" alt="Scroll Up">
        </div>
    </div>
</div>
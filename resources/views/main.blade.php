<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description">

    <link href="{{ asset('/images/icons/favicon.png') }}" rel="icon" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ URL::asset('/images/icons/favicon-lg.png') }}"/>
    <title>@yield('title')</title>
    @include('modules.headScripts')
</head>
<body class="@yield('bodyClass')">
    <header class="header">
        <div class="background-layer"></div>
        @include('modules.header')
        {{-- @include('modules.navigation') --}}
    </header>
    <main class="@yield('mainClass')">
        @yield('content')
    </main>
    <footer class="footer">
        @include('modules.footer')
    </footer>
    @include('modules.footScripts')
</body>
</html>
<form action="contact.php" method="POST" class="contact-form" id="contact-form" autocomplete="on" data-focus="false">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group label-floating" id="first_name-group">
                <label for="first_name" class="control-label">First Name</label>
                <input type="text" name="first_name" id="first_name" class="form-control"
                placeholder="e.g. John" required data-error="Required">
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group label-floating" id="last_name-group">
                <label for="last_name" class="control-label">Last Name</label>
                <input type="text" name="last_name" id="last_name" class="form-control"
                placeholder="e.g. Smith" required data-error="Required">
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group label-floating" id="email-group">
                <label for="email" class="control-label">Email Address</label>
                <input type="email" id="email" name="email" class="form-control"
                placeholder="e.g. example@gmail.com" required
                data-error="Enter a valid email address">
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 padding-zero">
            <div class="form-group label-floating">
                <label for="home_message" class="control-label">Comment Here</label>
                <div class="help-block with-errors"></div>
                <textarea rows="6" cols="40" id="home_message" name="message" class="form-control"
                placeholder="Your Message" data-error="Enter Your Message"
                required></textarea>
            </div>
            <button type="submit" class="btn-purple btn-md btn-block">Send Message</button>
        </div>
    </div>
</form>
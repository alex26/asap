@extends('main')

@section('title') Home @stop

@section('bodyClass') home-page @stop

@section('content')
    <section id="hero" class="d-flex align-items-end">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Need home <br />maintenance? <br /><span>Your job is our <br />priority.</span></h1>
                </div>
                <div class="col-12">
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <p>Handyman in Vancouver, WA <br class="d-sm-none"/>at your service.</p>
                        </div>
                        <div class="col-sm-6">
                            <a href="tel:3608367200"><img src="{{ asset('/images/icons/phone-icon.svg') }}" alt="Phone Icon">(360) 836-7200</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <section id="process">
        <div class="container">
            <div class="row">
                <img class="d-sm-none" src="{{ asset('/images/icons/scroll-down-icon-xs.svg') }}" alt="Scroll Down">
                <div class="col-sm-3">
                    <img class="d-none d-sm-block" src="{{ asset('/images/icons/scroll-down-icon.svg') }}" alt="Scroll Down">
                </div>
                <div class="col-auto m-auto col-sm-3">
                    <div class="row">
                        <span class="col-auto col-sm-12 col-lg-auto">01</span>
                        <p class="col">Mauris rhoncus orci in imperdiet placerat. </p>
                    </div>
                </div>
                <div class="col-auto m-auto col-sm-3">
                    <div class="row">
                        <span class="col-auto col-sm-12 col-lg-auto">02</span>
                        <p class="col">Mauris rhoncus orci in imperdiet placerat. </p>
                    </div>
                </div>
                <div class="col-auto m-auto col-sm-3">
                    <div class="row">
                        <span class="col-auto col-sm-12 col-lg-auto">03</span>
                        <p class="col">Mauris rhoncus orci in imperdiet placerat. </p>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="d-none d-lg-block col-lg-7 order-lg-last">
                    <div class="sticky">
                        <?php echo file_get_contents("../public/images/home/house-demo.svg"); ?>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <h2><small>Services</small>Here’s what we <br />can do for you.</h2>
                    <div class="d-lg-none">
                            <div class="sticky">
                                <?php echo file_get_contents("../public/images/home/house-demo.svg"); ?>
                            </div>
                        </div>
                    <div class="col-custom">
                        <img src="{{ asset('/images/home/services-icon-1.svg') }}" alt="Icon">
                        <h4>Gutter Cleaning & Repair</h4>
                        <p>Living in the Pacific Northwest, we understand the importance of a well-functioning drainage system. Clogged gutters are the primary cause of home water damage, and uncontrolled rain flow can erode your roof, siding, and landscape. We provide full gutter service including debris removal, downspout flushing, and screen installation.</p>
                    </div>
                    <div class="col-custom">
                        <img src="{{ asset('/images/home/services-icon-2.svg') }}" alt="Icon">
                        <h4>Roof Cleaning & Moss Treatment</h4>
                        <p>It is important to remove moss from your roof in order to preserve your home's beauty and to prevent irreparable damage. We begin by removing moss growths with a stiff brush and a blower. Then we apply granular zinc sulfate to the roof to kill any remaining moss. It is EPA approved and won't harm your plants or siding. Rain and wind naturally wash away any remaining dead moss.</p>
                    </div>
                    <div class="col-custom">
                        <img src="{{ asset('/images/home/services-icon-3.svg') }}" alt="Icon">
                        <h4>Deck / Fence Restoration & Staining</h4>
                        <p>In rain-soaked Washington, old decks often need help. Wet wood, especially if not properly treated and maintained, will break-down and rot over time. It’s important to take care of damage early–before a minor repair becomes a major expense! Keep your old fence or deck, make them look new!</p>
                    </div>
                    <div class="col-custom toggle-columns">
                        <img src="{{ asset('/images/home/services-icon-4.svg') }}" alt="Icon">
                        <h4>Indoor / Outdoor Painting</h4>
                        <p>When painting your home there are many factors to consider: selecting a color that will complement your home and picking a paint that will last for many years to come is crucial. The weather in the Pacific Northwest puts great demands on the exterior of your home. We provide professional quality painting to any interior or exterior surface.</p>
                    </div>
                    <div class="col-custom toggle-columns">
                        <img src="{{ asset('/images/home/services-icon-4.svg') }}" alt="Icon">
                        <h4>Basic Plumbing</h4>
                        <p>Need to change a dishwasher or a washing machine? Clogged sewer? Leaking faucet? Leave all the dirty work to us. We offer basic plumbing services such as maintenance, replacements, diagnostics, and repairs.</p>
                    </div>
                    <div class="col-custom toggle-columns">
                        <img src="{{ asset('/images/home/services-icon-4.svg') }}" alt="Icon">
                        <h4>Electrical</h4>
                        <p>If you need to change sockets, hang a chandelier, etc — we can help. We handle the installation and replacement of electrical appliances, switches, sockets, lamps, hairdryers, etc. Basically, if it can shock you, we know how to handle it.</p>
                    </div>
                    <div class="col-custom toggle-columns">
                        <img src="{{ asset('/images/home/services-icon-4.svg') }}" alt="Icon">
                        <h4>Holiday & Event Lightning</h4>
                        <p>If you are tired of installing and removing your own Christmas and other holiday lights every year, we can help! We even handle special events and occasions. Rooflines, shrubs, trees, and yard decorations are our specialty.</p>
                    </div>
                    <div class="toggle-button">
                        <img src="{{ asset('/images/icons/plus-icon.svg')}}"> More Services
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cta">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-6 col-xl-5 offset-lg-1 offset-xl-2">
                    <h3>Your job is our priority.</h3>
                </div>
                <div class="col-md-5 col-lg-4 col-xl-3">
                    <a href="#">Request Maintenance</a>
                </div>
            </div>
        </div>
    </section>

    <section id="works">
        <div class="container">
            <div class="row">
                <h2><small>Works</small>All the dirty work <br class="d-none d-md-block" />is on us!</h2>
                <div class="bunch-images">
                    <img src="{{ asset('/images/home/image-1.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-2.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-3.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-4.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-5.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-6.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-7.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-8.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-9.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-10.jpg') }}" alt="Image">
                    <img src="{{ asset('/images/home/image-11.jpg') }}" alt="Image">
                </div>
                <p>Looking for a quality service you can rely on? Look no further. We're ASAP Maintenance, available where you need us when you need us.</p>
            </div>
        </div>
    </section>

    <section id="about_contact">
        <div class="fill-content"></div>
        <div class="fill-content yellow"></div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-top d-sm-none">
                    <h2><small>About</small>Working hard <br />with a grateful heart.</h2>
                    <img class="family-picture" src="{{  asset('/images/home/avatar-img.jpg') }}" alt="Image">
                    <img class="signature-icon" src="{{  asset('/images/home/signature-icon.svg') }}" alt="Signature">
                    <p>My name is Roman. <br />In the 2016 year, my family moved to the United States from Russia. We are grateful for the opportunity to live and work in this blessed country. While still living in Russia, I earned my daily bread with my own hands, hence the reason I'm not afraid of hard, and sometimes dirty work. Moreover, I believe that physical labor helps make a person better.</p>
                    <p>I love what I do, and I love to do it well. I work hard to ensure that I am not ashamed before my family or before God!</p>
                    <p>If you need fast and quality work — you know who to ask… ASAP MAINTENANCE.</p>
                </div>
                <div class="col-left d-none d-sm-block col-sm-5 col-lg-5 col-xl-4">
                    <img src="{{  asset('/images/home/avatar-img.jpg') }}" alt="Image">
                    <img class="signature-icon" src="{{  asset('/images/home/signature-icon.svg') }}" alt="Signature">
                    <p>To contact us, you can write us a message, briefly explaining your need. We will reply to you ASAP.</p>
                    <p>You can also call us at the phone number listed below.</p>
                    <div class="time">
                        <h4>Working hours:</h4>
                        <p>Monday – Friday</p>
                        <p>7:00 AM – 7:00 PM</p>
                        <h4 class="fill-black">Feel free to call</h4>
                        <a href="tel:3608367200"><img src="{{ asset('images/icons/phone-icon-2.svg') }}" alt="Phone Icon">(360) 836-7200</a>
                    </div>
                </div>
                <div class="col-right col-12 col-sm-7 col-lg-6 col-xl-7 offset-lg-1">
                    <h2 class="d-none d-sm-block"><small>About</small>Working hard <br />with a grateful heart.</h2>
                    <p class="d-none d-sm-block">My name is Roman. <br />In the 2016 year, my family moved to the United States from Russia. We are grateful for the opportunity to live and work in this blessed country. While still living in Russia, I earned my daily bread with my own hands, hence the reason I'm not afraid of hard, and sometimes dirty work. Moreover, I believe that physical labor helps make a person better.</p>
                    <p class="d-none d-sm-block">I love what I do, and I love to do it well. I work hard to ensure that I am not ashamed before my family or before God!</p>
                    <p class="d-none d-sm-block">If you need fast and quality work — you know who to ask… ASAP MAINTENANCE.</p>

                    <div class="contact-form-component" id="contact">
                        <h3><small>Contact</small>Let's get in touch.</h3>
                        <form action="">
                            <div class="row">
                                <div class="col-12 col-md">
                                    <div class="form-group label-floating">
                                        <label for="first_name" class="control-label">First Name</label>
                                        <input type="text" id="first_name" class="form-control" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-12 col-md">
                                    <div class="form-group label-floating">
                                        <label for="email" class="control-label">Email Address</label>
                                        <input type="text" id="email" class="form-control" placeholder="Email Address">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group label-floating">
                                <label for="subject" class="control-label">Subject</label>
                                <input type="text" id="subject" class="form-control" placeholder="Subject">
                            </div>
                            <div class="form-group label-floating">
                                <label for="message" class="control-label">Your Message</label>
                                <textarea name="message" rows="3" cols="80" class="form-control" placeholder="Your Message"></textarea>
                            </div>
                            <div class="row align-items-center">
                                <div class="col order-md-last">
                                    <button type="submit" class="btn btn-black btn-md">Submit Enquiry</button>
                                </div>
                                <div class="col-12 col-md">
                                    <p>We don’t share your personal <br />information with anyone. Promise.</p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 d-sm-none time">
                    <h4>Working hours:</h4>
                    <p>Monday – Friday</p>
                    <p>7:00 AM – 7:00 PM</p>
                    <h4 class="fill-black">Feel free to call</h4>
                    <a href="tel:3608367200"><img src="{{ asset('images/icons/phone-icon-2.svg') }}" alt="Phone Icon">(360) 836-7200</a>
                </div>
            </div>
        </div>
    </section>
@endsection